//

import Combine
//  ContentView.swift
//  CryptoWalletCombine
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//
import SwiftUI
struct ContentView: View {
    @ObservedObject private var wallet = WalletListViewModel()
    @State var isPressed: Bool = false
    @State var search: String
    var body: some View {
        NavigationView {
            ScrollView(.vertical) {
                VStack {
                    SearchBarView(searchText: self.$search)
                        .padding()
                    ForEach(wallet.coin.filter {
                        search.isEmpty ? true : $0.name.contains(search)
                    }, id: \.self) {
                        items in
                        Group {
                            CardView(history: items.history, nameStock: items.name, marketCap: items.marketCap, volume: items.volume, currentPrice: items.price, imageUrl: items.image, nameFormat: items.displayText, symbol: items.symbol, isPressed: self.isPressed)
                        }.padding(5)
                    }
                }

                Spacer()
            }

            .onAppear {
                self.wallet.fetchCoins()
            }
            .navigationBarTitle("Crypto Price")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(search: "")
    }
}
