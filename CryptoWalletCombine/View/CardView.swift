//
//  CardView.swift
//  CryptoWalletCombine
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Foundation
import SwiftUI

struct CardView: View {
    var history: [Double]
    var nameStock: String
    var marketCap: UInt64
    var volume: UInt64
    var currentPrice: String
    var imageUrl: String
    var nameFormat: String
    var symbol: String
    @State var isPressed = false

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                ImageView(withURL: imageUrl)
                    .padding()

                VStack(alignment: .leading) {
                    Text(nameFormat)
                        .font(.custom("Gill Sans", size: 20))
                        .fontWeight(.medium)
                    Text("(\(symbol))")
                        .font(.custom("Gill Sans", size: 15))
                        .fontWeight(.semibold)

                }.padding(.leading, 40.0)
                Spacer()
                Button(action: {
                    self.isPressed.toggle()

                }, label: {
                    Image(systemName: "flame")
                        .font(.system(size: 40, weight: .medium, design: .rounded))
                        .padding()
                })
                    .sheet(isPresented: $isPressed) {
                        WalletDetailView(imageUrl: self.imageUrl, nameStock: self.nameStock, currentPrice: self.currentPrice, symbol: self.symbol, volume: self.volume, marketCap: self.marketCap, history: self.history, isOn: self.$isPressed)
                    }.transition(.move(edge: .top))
            }
            .frame(width: 400, height: 80)
            .background(Color.pink)
            .cornerRadius(35)

        }.padding(.horizontal, 10.0)
    }
}
