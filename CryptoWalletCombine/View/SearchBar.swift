//
//  SearchBar.swift
//  CryptoWalletCombine
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Foundation
import SwiftUI

struct SearchBarView: View {
    @Binding var searchText: String
    @State private var showCancelButton: Bool = false
    var onCommit: () -> Void = { print("onCommit") }

    var body: some View {
        HStack {
            HStack {
                Image(systemName: "magnifyingglass")

                ZStack(alignment: .leading) {
                    if searchText.isEmpty {
                        Text("Search")
                    }
                    TextField("", text: $searchText, onEditingChanged: { _ in
                        self.showCancelButton = true
                    }, onCommit: onCommit).foregroundColor(.primary)
                }

                Button(action: {
                    self.searchText = ""
                }) {
                    Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                }
            }
            .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
            .foregroundColor(.secondary)
            .background(Color(.tertiarySystemFill))
            .cornerRadius(10.0)

            if showCancelButton {
                Button("Cancel") {
                    UIApplication.shared.endEditing(true)
                    self.searchText = ""
                    self.showCancelButton = false
                }
                .foregroundColor(Color(.systemBlue))
            }
        }
        .padding(.horizontal)
        .navigationBarHidden(showCancelButton)
    }
}
