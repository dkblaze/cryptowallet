//
//  SwiftUIView.swift
//  CryptoWalletCombine
//
//  Created by Chris on 9/1/20.
//  Copyright © 2020 Blaze. All rights reserved.
//

import SwiftUI

struct WalletDetailView: View {
    var imageUrl: String
    var nameStock: String
    var currentPrice: String
    var symbol: String
    var volume: UInt64
    var marketCap: UInt64
    var history: [Double]
    @Binding var isOn: Bool

    var body: some View {
        VStack(alignment: .center, spacing: 3) {
            Button(action: {
                self.isOn.toggle()
            }, label: {
                HStack {
                    Spacer()
                        .frame(width: 350)
                    Image(systemName: "xmark.octagon")
                        .font(.system(size: 45, weight: .heavy, design: .rounded))
                        .padding([.top, .trailing], 10.0)
                        .foregroundColor(Color.red)
                }
            })
            Text(self.nameStock)
                .font(.custom("Gill Sans", size: 35))
                .fontWeight(.medium)
                .padding()
            ImageView(withURL: self.imageUrl)
                .font(.system(size: 50, weight: .bold, design: .rounded))
                .padding(20)
                .background(Color.white)
                .cornerRadius(35)
                .padding(.all, 15)
                .background(Color.red)
                .cornerRadius(35)
            Spacer()
                .frame(height: 30)
            VStack(alignment: .center) {
                Text("Current Price - $\(self.currentPrice))")
                    .font(.custom("Gill Sans", size: 22))
                    .fontWeight(.regular)
                    .padding(3)
                Text("Symbol - \(self.symbol)")
                    .font(.custom("Gill Sans", size: 19))
                    .fontWeight(.regular)
                    .padding(3)
                Text("Volume - \(self.volume)")
                    .font(.custom("Gill Sans", size: 19))
                    .fontWeight(.regular)
                    .padding(3)
                Text("MarketCap - \(self.marketCap)")
                    .font(.custom("Gill Sans", size: 19))
                    .fontWeight(.regular)
                    .padding(3)
            }
            LineView(data: self.history, title: "Hirsory Price")
                .padding(.all, 20)
        }.padding(.all, 10)
    }
}
