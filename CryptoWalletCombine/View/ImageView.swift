//
//  ImageView.swift
//  CryptoWallet
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Combine
import SwiftUI

struct ImageView: View {
    @ObservedObject var imageLoader: ImageLoader
    @State var image = UIImage()
    init(withURL url: String) {
        imageLoader = ImageLoader(urlString: url)
    }

    var body: some View {
        Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .frame(width: 50, height: 50)
            .padding(5)
            .background(Color.white)
            .cornerRadius(25)
            .padding(.all, 4)
            .onReceive(imageLoader.dataPublisher) { data in
                self.image = UIImage(data: data) ?? UIImage()
            }
    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ImageView(withURL: "https://cdn.coinranking.com/Sy33Krudb/btc.png")
            ImageView(withURL: "https://cdn.coinranking.com/Sy33Krudb/btc.png")
        }
    }
}
