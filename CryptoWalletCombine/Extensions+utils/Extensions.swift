//
//  Extensions.swift
//  CryptoWalletCombine
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Foundation
import SwiftUI

extension View {
    func resignKeyboardOnDragGesture() -> some View {
        modifier(ResignKeyboardOnDragGesture())
    }
}

extension UIApplication {
    func endEditing(_ force: Bool) {
        windows
            .filter { $0.isKeyWindow }
            .first?
            .endEditing(force)
    }
}
