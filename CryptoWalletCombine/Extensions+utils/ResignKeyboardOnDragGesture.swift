//
//  ResignKeyboardOnDragGesture.swift
//  CryptoWalletCombine
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Foundation
import SwiftUI

struct ResignKeyboardOnDragGesture: ViewModifier {
    var gesture = DragGesture().onChanged { _ in
        UIApplication.shared.endEditing(true)
    }

    func body(content: Content) -> some View {
        content.gesture(gesture)
    }
}
