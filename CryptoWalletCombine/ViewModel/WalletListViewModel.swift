//
//  CryptoListViewModel.swift
//  CryptoWallet
//
//  Created by Chris .
//  Copyright © 2020 Blaze. All rights reserved.
//

import Combine
import Foundation

class WalletListViewModel: ObservableObject {
    @Published var coin = [WalletViewModel]()

    private let walletService = WalletNetowrking()
    var cancellable: AnyCancellable?

    func fetchCoins() {
        cancellable = walletService.fetchCoin()
            .sink(receiveCompletion: { _ in }, receiveValue: { container in
                self.coin = container.data.coins.map { WalletViewModel($0) }
                print(self.coin)
            })
    }
}
