//
//  ImageLoader.swift
//  CryptoWallet
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Combine
import Foundation

class ImageLoader: ObservableObject {
    var dataPublisher = PassthroughSubject<Data, Never>()
    var data = Data() {
        didSet {
            dataPublisher.send(data)
        }
    }

    init(urlString: String) {
        guard let url = URL(string: urlString)
        else { return }
        let task = URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data
            else { return }
            DispatchQueue.main.async {
                self.data = data
            }
        }
        task.resume()
    }
}
