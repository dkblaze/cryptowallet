//
//  CryptoNetworking.swift
//  CryptoWallet
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Combine
import Foundation

final class WalletNetowrking {
    var components: URLComponents {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "api.coinranking.com"
        components.path = "/v1/public/coins"
        components.queryItems = [URLQueryItem(name: "base", value: "USD"), URLQueryItem(name: "timePeriod", value: "24h")]

        return components
    }

    func fetchCoin() -> AnyPublisher<Wallet, Error> {
        return URLSession.shared.dataTaskPublisher(for: components.url!)
            .map { $0.data }
            .decode(type: Wallet.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
