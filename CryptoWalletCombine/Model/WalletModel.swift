//
//  WalletModel.swift
//  CryptoWallet
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Foundation

struct Wallet: Codable {
    let status: String
    let data: Coin
}

struct Coin: Codable {
    let coins: [WalletData]
}

struct WalletData: Codable, Hashable {
    let name: String
    let price: String
    let symbol: String

    let iconUrl: String
    let history: [String?]
    let marketCap: UInt64
    let volume: UInt64
}
