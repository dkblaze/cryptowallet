//
//  CryptoViewModel.swift
//  CryptoWallet
//
//  Created by Chris
//  Copyright © 2020 Blaze. All rights reserved.
//

import Combine
import Foundation
import SwiftUI

struct WalletViewModel: Hashable {
    private let coin: WalletData

    var name: String {
        return coin.name
    }

    var formattedPrice: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency

        guard let price = Double(coin.price), let forrmatedPrice = numberFormatter.string(from: NSNumber(value: price)) else { return "" }

        return forrmatedPrice
    }

    var displayText: String {
        return name + " - " + formattedPrice
    }

    var symbol: String {
        return coin.symbol
    }

    var image: String {
        return coin.iconUrl.replacingOccurrences(of: "svg", with: "png")
    }

    var marketCap: UInt64 {
        return coin.marketCap
    }

    var volume: UInt64 {
        return coin.volume
    }

    var history: [Double] {
        var result = [Double]()
        for value in coin.history {
            if let stringValue = value, let doubleValue = Double(stringValue) {
                result.append(doubleValue)
            }
        }
        return result
    }

    var price: String {
        return coin.price
    }

    init(_ coin: WalletData) {
        self.coin = coin
    }
}
